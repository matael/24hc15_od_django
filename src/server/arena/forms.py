# -*- coding:utf8 -*-

from django import forms

from arena.models import Team, Bot

class TeamForm(forms.Form):

    name = forms.CharField(label="Nom de l'équipe", max_length=200)


class BotForm(forms.Form):

    name = forms.CharField(label="Nom du bot", max_length=200)
    team_secret = forms.CharField(label="Mot de passe", max_length=100)
