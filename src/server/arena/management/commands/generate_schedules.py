#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# generate_schedules.py
#
# Copyright © 2015 Mathieu Gaborit (matael) <mathieu@matael.org>
#
#
# Distributed under WTFPL terms
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
#

from __future__ import print_function

from django.core.management.base import BaseCommand
from arena.models import Stop, Track, ChainedStop, Passage

import os
import sys
import time
import string
import datetime

from random import randint

def xsum(v):
    t = datetime.timedelta(minutes=0)
    for _ in v: t=t+_
    return t

class Command(BaseCommand):
    help = "Generate new schedule for each track"


    def handle(self, *a, **kw):

        before = time.time()
        errors = 0

        tracks = Track.objects.all()
        n = tracks.count()

        for t in tracks:

            n_stops = t.stops.count()

            # generate n-1 Δt for all the stops but the first one
            # 3min < Δt < 8min
            # initial zero is for the first stop
            delta_t = [datetime.timedelta(minutes=0)] + [datetime.timedelta(minutes=randint(3,8)) for _ in xrange(n_stops-1)]


            for day_type in ['weekday', 'sat', 'sun']:
                # opening time between 5.30am and 7.30 on weekdays
                if t.track_number[0]=='T':
                    if day_type=='sun':
                        hour=randint(5,6)
                        minute=randint(0,30)
                        if hour==5: minute += 30
                    else:
                        hour=randint(4,5)
                        minute=randint(0,30)
                        if hour==4: minute += 30
                else:
                    if day_type=='sun':
                        hour=randint(7,8)
                        minute=randint(0,30)
                        if hour==5: minute += 30
                    else:
                        hour=randint(5,7)
                        minute=randint(0,30)
                        if hour==5: minute += 30

                if minute==60: minute-=1
                opening_time = datetime.datetime.combine(datetime.date.today(), datetime.time(
                        hour=hour,
                        minute=minute
                ))

                # closing time between 8pm and 11pm on weekdays
                if t.track_number[0]=='T':
                    hour = 23
                else:
                    if day_type=='sun':
                        hour=randint(19,20)
                    else:
                        hour=randint(20,22)
                minute=randint(0,59)

                closing_time = datetime.datetime.combine(datetime.date.today(), datetime.time(
                        hour=hour,
                        minute=minute
                ))

                # generate schedule matrix
                # m index for passage
                # n index for stops
                # r repeat factor
                if t.track_number[0]=='T':
                    if day_type=='weekday':
                        r = randint(5,7)
                    elif day_type=='sat':
                        r = randint(6,9)
                    else:
                        r = randint(10,20)
                else:
                    if day_type=='weekday':
                        r = randint(10,25)
                    elif day_type=='sat':
                        r = randint(10,30)
                    else:
                        r = randint(20,45)

                schedule_spread = closing_time- opening_time - xsum(delta_t)
                schedule_spread = schedule_spread.total_seconds()/60
                max_m = int(schedule_spread/r)
                self.stdout.write('\t Opening time : {}'.format(opening_time.time().isoformat()))
                self.stdout.write('\t Closing time : {}'.format(closing_time.time().isoformat()))
                self.stdout.write('\t Schedule Spread : {}'.format(schedule_spread/60))
                self.stdout.write('\t Time to traverse : {}'.format(xsum(delta_t)))
                self.stdout.write('\tRepeat factor : {} minutes'.format(r))
                self.stdout.write('\tRepeat number : {} times'.format(max_m))

                schedule_matrix = [
                    [
                        opening_time+m*datetime.timedelta(minutes=r)+xsum(delta_t[:n+1])+datetime.timedelta(minutes=randint(0,1))
                        for m in xrange(max_m)
                    ]
                    for n in xrange(n_stops)
                ]


                schedule_matrix = [[_.time() for _ in l] for l in schedule_matrix]

                self.stdout.write('=> Loading schedule for track {}'.format(t.track_number))
                if day_type=='weekday':
                    days = [_[0] for _ in Passage.DAYS[:5]]
                if day_type=='sat':
                    days = [Passage.DAYS[5][0]]
                else:
                    days = [Passage.DAYS[6][0]]

                for n,s in enumerate(ChainedStop.objects.filter(track=t).order_by('position')):
                    for pt in schedule_matrix[n]:
                        for day in days:

                            p = Passage(time=pt, day=day, stop=s)
                            p.save()

                self.stdout.write('.... DONE')

        elapsed = time.time()-before

        self.stdout.write("{}/{} track loaded without errors in {}s.".format(n-errors,n, elapsed))

