# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('arena', '0007_move_track'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot2game',
            name='position',
            field=models.ForeignKey(related_name='position', default=1390, to='arena.Stop'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bot2game',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 16, 0, 53, 7, 3771)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='move',
            name='label',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='bot2game',
            name='target',
            field=models.ForeignKey(related_name='target', to='arena.Stop'),
            preserve_default=True,
        ),
    ]
