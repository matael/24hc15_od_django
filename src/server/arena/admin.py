from django.contrib import admin

from arena.models import *

class StopAdmin(admin.ModelAdmin):
    list_display = ('name',)

class TrackAdmin(admin.ModelAdmin):
    list_display = ('track_number', 'direction', 'num_of_stops', 'service')

class ChainedStopAdmin(admin.ModelAdmin):
    list_display = ('stop', 'track', 'position')
    list_filter = ['track__track_number']

class PassageAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'time', 'day', 'stop', 'stop')
    list_filter = ['day', 'stop__track__track_number']

admin.site.register(Stop, StopAdmin)
admin.site.register(Track, TrackAdmin)
admin.site.register(ChainedStop, ChainedStopAdmin)
admin.site.register(Passage, PassageAdmin)
admin.site.register(Shortcut)
admin.site.register(Team)
admin.site.register(Bot)
admin.site.register(Game)
admin.site.register(Bot2Game)
admin.site.register(Incident)
admin.site.register(Move)
