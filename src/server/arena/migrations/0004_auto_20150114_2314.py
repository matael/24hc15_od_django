# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('arena', '0003_auto_20150111_2237'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot2game',
            name='rerouted',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stop',
            name='lat',
            field=models.DecimalField(default=0, max_digits=13, decimal_places=11),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stop',
            name='lon',
            field=models.DecimalField(default=0, max_digits=13, decimal_places=11),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='move',
            name='from_stop',
            field=models.ForeignKey(related_name='move_from', to='arena.ChainedStop'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='move',
            name='to_stop',
            field=models.ForeignKey(related_name='move_to', to='arena.ChainedStop'),
            preserve_default=True,
        ),
    ]
