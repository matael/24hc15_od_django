#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# load_timeo.py
#
# Copyright © 2015 Mathieu Gaborit (matael) <mathieu@matael.org>
# # # Distributed under WTFPL terms
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
#

from django.core.management.base import BaseCommand
from arena.models import Stop, Track, ChainedStop, Passage

import os
import sys
import time
import string

class Command(BaseCommand):
    help = "Loads stops from Timeo scrapping"


    def handle(self, *a, **kw):

        BASEDIR = '../../scripts/load_stops_data/'
        files = [_ for _ in os.listdir(BASEDIR) if _[0]!='_']

        self.stdout.write('=> Loading stops from {}*'.format(BASEDIR))
        stops = []
        for f in files:

            with open(BASEDIR+f) as fh:

                lines = fh.readlines()
                plines = [filter(lambda x: x in string.printable, _) for _ in lines]
                track_num = lines[0].split(':')[1].rstrip()
                sens = lines[1].split(':')[1].rstrip()
                dest = lines[2].split(':')[1].rstrip()

                ptrack_num = plines[0].split(':')[1].rstrip()
                psens = plines[1].split(':')[1].rstrip()
                pdest = plines[2].split(':')[1].rstrip()

                try:
                    stops += [_.split(':')[1].rstrip() for _ in lines[3:]]
                except IndexError:
                    print('[ERROR] IndexError on track {}_{}'.format(ptrack_num, psens))

                self.stdout.write('\tTrack : {0}_{1} -> {2}'.format(ptrack_num, psens, pdest))

        self.stdout.write('=> Results:\n\tBefore reduction : {} stops'.format(len(stops)))
        unique_stops = list(set(stops))
        self.stdout.write('\tAfter reduction : {} unique stops'.format(len(unique_stops)))

        before = time.time()
        errors = 0

        ans = raw_input('Add to database [N/o] ? ')
        if ans.lower()!='o':
            self.stdout.write('Aborting...')
            sys.exit(0)

        self.stdout.write('=> Attempting to add {} stops to database'.format(len(unique_stops)))

        for s in unique_stops:

            try:
                tmp_s = Stop(name=s.capitalize())
                tmp_s.save()

            except:
                self.stdout.write('[ERROR] Loading of {} failed'.format(s))
                error+=1


        elapsed = time.time()-before

        self.stdout.write("{}/{} stops loaded without errors in {}s.".format(len(unique_stops)-errors,len(unique_stops), elapsed))




