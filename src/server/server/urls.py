from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'server.views.home', name='home'),
    url(r'^api/', include('arena.urls')),

    url(r'^admin/', include(admin.site.urls)),
    staticfiles_urlpatterns(),

)
