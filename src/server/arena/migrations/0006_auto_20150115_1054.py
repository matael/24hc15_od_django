# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('arena', '0005_auto_20150115_0915'),
    ]

    operations = [
        migrations.AlterField(
            model_name='passage',
            name='stop',
            field=models.ForeignKey(to='arena.ChainedStop'),
            preserve_default=True,
        ),
    ]
