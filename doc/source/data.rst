Données
=======

Les données fournies sont au format JSON, à raison d'un fichier par ligne de bus/tram.

Dans chaque fichier on retrouve :

.. code-block:: javascript

        {
                'track_id': identifiant,
                'track_number': numero_de_ligne,
                'n_stops': nombre_d_arrets,
                'track_dest': {'id': id_de_l_arret_de_destination, 'name': nom_de_l_arret_de_destination},
                'stops':
                        [
                            {
                                'id': id_de_l_arret,
                                'name': nom_de_l_arret,
                                'position': position_sur_la_ligne
                            },
                            // ....
                        ],
                'schedule': {
                    id_de_l_arret: {
                        abbrev_du_jour: [ "HH:MM:SS", .... ] // abbrev : lu ma me je ve sa di
                    }
                }
        }

Les fichiers sont nommés ``numero_de_ligne.json``.
