#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# load_stops_tracks.py
#
# Copyright © 2015 Mathieu Gaborit (matael) <mathieu@matael.org>
#
#
# Distributed under WTFPL terms
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
#

from pytimeo import Timeo

# data folder
DATA = './load_stops_data/'
T = Timeo()

tracks = T.get_lignes()

for long_name,num in tracks.items():
    filename = DATA+num

    print 'Processing track {}'.format(long_name.encode('utf8'))
    print('\t\toutput: {}'.format(filename))

    stops = T.getall_arrets(num)

    print('\t\tstops number: {}'.format(len(stops)))

    tmp = long_name.split(' ')[3:]
    destname = ' '.join(tmp)

    sens = num.split('_')[1]
    tracknum = num.split('_')[0]

    with open(filename, 'w') as out:
        out.write('TrackNum:{}\n'.format(tracknum))
        out.write('Sens:{}\n'.format(sens))
        out.write('DestName:{}\n'.format(destname.encode('utf8')))

        for timeocode,stop_name in stops.items():
            out.write('{0}:{1}\n'.format(timeocode,stop_name.encode('utf8')))
