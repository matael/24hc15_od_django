# -*- coding:utf8 -*-

from datetime import timedelta
from hashlib import md5, sha256
import time
import datetime
import random

from django.conf import settings
from django.db import models as m


class Stop(m.Model):

    name = m.CharField(max_length=50)
    lat = m.DecimalField(default=0, max_digits=13,decimal_places=11)
    lon = m.DecimalField(default=0, max_digits=13,decimal_places=11)

    def __unicode__(self):
        return str(self.id)+") "+self.name

class Track(m.Model):

    track_number = m.CharField(max_length=4)
    direction = m.ForeignKey(Stop, related_name="direction_of")
    service = m.CharField(max_length=30, default="SETRAM", blank=True)
    stops = m.ManyToManyField(Stop, through='ChainedStop', through_fields=('track', 'stop'))

    def __unicode__(self):
        return "["+self.service+"] Track no. "+str(self.track_number)+" -> "+self.direction.__unicode__()

    def num_of_stops(self):
        return self.stops.count()


class ChainedStop(m.Model):

    stop = m.ForeignKey(Stop)
    track = m.ForeignKey(Track)
    position = m.IntegerField()

    def __unicode__(self):

        return '['+self.track.track_number+'] '+self.stop.__unicode__()


class Passage(m.Model):

    LUNDI = 'lu'
    MARDI = 'ma'
    MERCREDI = 'me'
    JEUDI = 'je'
    VENDREDI = 've'
    SAMEDI = 'sa'
    DIMANCHE = 'di'

    DAYS = (
        (LUNDI, 'Lundi'),
        (MARDI, 'Mardi'),
        (MERCREDI, 'Mercredi'),
        (JEUDI, 'Jeudi'),
        (VENDREDI, 'Vendredi'),
        (SAMEDI, 'Samedi'),
        (DIMANCHE, 'Dimanche'),
    )

    time = m.TimeField()
    day = m.CharField(choices=DAYS, max_length=9)
    stop = m.ForeignKey(ChainedStop)

    def __unicode__(self):
        return self.stop.__unicode__()+":"+self.day+"@"+self.time.isoformat()


class Shortcut(m.Model):

    timelength = m.IntegerField()
    from_stop = m.ForeignKey(Stop, related_name="shortcut_from")
    to_stop = m.ForeignKey(Stop, related_name="shortcut_to")

    def __unicode__(self):
        return "Shortcut: "+self.from_stop+" -> "+self.to_stop+" ("+self.timelength+")"


class Team(m.Model):

    name = m.CharField(max_length=200)
    team_secret = m.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.name

    def generate_credentials(self):

        secret_prime = "TEAM"+self.name+str(time.time())+settings.SECRET_KEY
        self.team_secret = sha256(secret_prime.encode('utf8')).hexdigest()

        return self.team_secret


class Bot(m.Model):

    OFFLINE = 'offline'
    READY = 'ready'
    INGAME = 'ingame'

    STATES = (
        (OFFLINE, 'Offline'),
        (READY, 'Ready'),
        (INGAME, 'In Game')
    )

    name = m.CharField(max_length=200)
    token = m.CharField(max_length=100)
    secret = m.CharField(max_length=100)
    state = m.CharField(max_length=7, choices=STATES, default='offline')
    last_move = m.DateTimeField(auto_now_add=True)
    team = m.ForeignKey(Team)

    def __unicode__(self):
        return self.name+"("+self.team.name+")"

    def generate_credentials(self):

        self.token = md5(self.name+self.team.name).hexdigest()
        secret_prime = self.token+str(time.time())+settings.SECRET_KEY
        self.secret = sha256(secret_prime.encode('utf8')).hexdigest()

        return (self.token, self.secret)

    def get_score(self):

        return sum([_.score for _ in Bot2Game.objects.filter(game__mode='arena', bot=self)])


class Game(m.Model):

    TRAINING = 'training'
    ARENA = 'arena'

    MODES = (
        (TRAINING, 'training'),
        (ARENA, 'arena')
    )


    PENDING = 'pending'
    STARTED = 'started'
    FINISHED = 'finished'

    STATES = (
        (PENDING, 'pending'),
        (STARTED, 'started'),
        (FINISHED, 'finished')
    )

    dtstart = m.DateTimeField()
    mode = m.CharField(choices=MODES, max_length=8)
    first_stop = m.ForeignKey(Stop, related_name='first_stop')
    initial_arrival = m.ForeignKey(Stop, related_name='initial_arrival')
    game_token = m.CharField(max_length=100, unique=True)
    bots = m.ManyToManyField(Bot, through='Bot2Game', through_fields=('game', 'bot'))
    num_bots = m.IntegerField(default=0)
    timestamp = m.DateTimeField(auto_now_add=True)
    state = m.CharField(choices=STATES, max_length=8, default=PENDING)

    def _get_hash(self):
        return md5(self.__unicode__()+str(self.dtstart)).hexdigest()

    def __unicode__(self):
        return "["+self.mode+"] Start : "+self.first_stop.__unicode__()

    def save(self, *a, **kw):
        """ Override save to automatically generate game token """
        self.game_token = self._get_hash()
        super(Game, self).save(*a, **kw)

    @staticmethod
    def create_new(mode='training'):
        month = random.randint(1,12)
        if month in [1, 3, 5, 7, 8, 10, 12]:
            day = random.randint(1,31)
        if month==2:
            day = random.randint(1,28)
        else:
            day = random.randint(1,30)
        hour = random.randint(5,20)
        minutes = random.randint(0,59)
        dtstart = datetime.datetime(2015, month, day, hour, minutes, 0)
        stops = Stop.objects.all()
        idx = random.randint(0,stops.count()-1)
        first_stop = stops[idx]
        dist = 0
        while dist < 10:
            idx2 = random.randint(0,stops.count()-1)
            dist = abs(idx-idx2)
        initial_arrival = stops[idx2]

        g = Game(
            dtstart=dtstart,
            first_stop=first_stop,
            initial_arrival=initial_arrival,
            mode=mode,
            num_bots=0
        )
        g.save()
        return g

    def generate_incident(self, n=1):

        for j in xrange(n):

            i = Incident(game=self)
            # generate a random starttime between game start and 5h later
            i.dtstart = self.dtstart + datetime.timedelta(minutes=random.randint(0,5*60))
            i.dtend = i.dtstart + datetime.timedelta(minutes=random.randint(0,5*60))

            i.type = random.choice([_[0] for _ in Incident.TYPES])

            i.stop = random.choice(Stop.objects.all())
            i.save()

    def new_target(self):

        # first, get lines and stops we must avoid
        b2gs = Bot2Game.objects.filter(game=self, arrived=True)
        stops_to_avoid = [_.target.id for _ in b2gs] + [self.initial_arrival.id]
        b2gs_still_in_game = Bot2Game.objects.filter(game=self, arrived=False)
        tracks_to_avoid = []
        for b in b2gs_still_in_game:
            last_move = Move.objects.filter(bot=b.bot, game=self).order_by('-date_played')[0]
            tracks_to_avoid.append(last_move.to_stop.track.id)

        valid_track = False
        valid_stop = False
        while not valid_track:
            l = random.choice(Track.objects.all())
            if l.id not in tracks_to_avoid:
                valid_track = True

        while not valid_stop:
            s = random.choice(map(lambda _: _.stop, ChainedStop.objects.filter(track=l)))
            if s.id not in stops_to_avoid:
                valid_stop = True

        for b in b2gs_still_in_game:
            b.target = s
            b.rerouted += 1
            b.save()

        return s


class Bot2Game(m.Model):

    bot = m.ForeignKey(Bot)
    game = m.ForeignKey(Game)
    score = m.IntegerField(default=0)
    position = m.ForeignKey(Stop, related_name='position')
    time = m.DateTimeField()
    target = m.ForeignKey(Stop, related_name='target')
    arrived = m.BooleanField(default=False)
    rerouted = m.IntegerField(default=0)


class Incident(m.Model):

    PANNE = 'panne'
    MALAISE = 'malaise'
    ACCIDENT = 'accident'
    TRAVAUX = 'travaux'
    MANIF = 'manif'
    GREVE = 'greve'

    TYPES = (
        (PANNE, 'Panne'),
        (MALAISE, "Malaise d'un voyageur"),
        (ACCIDENT, 'Accident de la circulation'),
        (TRAVAUX, 'Travaux sur les voies'),
        (MANIF, 'Manifestation !'),
        (GREVE, 'Grève du transporteur')
    )

    PENALTIES = (
        (PANNE, 30),
        (MALAISE,10),
        (ACCIDENT,60),
        (TRAVAUX,120),
        (MANIF,120),
        (GREVE,480)
    )

    type = m.CharField(choices=TYPES, max_length=10)
    dtstart = m.DateTimeField()
    dtend = m.DateTimeField()

    game = m.ForeignKey(Game)
    stop = m.ForeignKey(Stop)

    def __unicode__(self):
        return self.type


class Move(m.Model):

    game = m.ForeignKey(Game)
    bot = m.ForeignKey(Bot)
    track = m.ForeignKey(Track)
    label = m.CharField(max_length=50)
    from_stop = m.ForeignKey(ChainedStop, related_name="move_from")
    to_stop = m.ForeignKey(ChainedStop, related_name="move_to")
    timelength = m.IntegerField()
    date_played = m.DateTimeField(auto_now_add=False)
    track = m.ForeignKey(Track)

    def __unicode__(self):
        return self.from_stop.stop.name+" -> "+self.to_stop.stop.name


