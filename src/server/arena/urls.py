from django.conf.urls import patterns, url

urlpatterns = patterns('arena.views',
    url(r'^register/?$', 'register', name='register'),
    url(r'^register/new_bot/?$', 'register_bot', name='register_bot'),
    url(r'^register/new_team/?$', 'register_team', name='register_team'),
    url(r'^connect/(?P<bot_token>[a-z0-9]+)/?$', 'connect', name='connect'),
    url(r'^play/(?P<game_token>[a-f0-9]+)/(?P<bot_token>[a-f0-9]+)/move/?$', 'move', name='move'),
    url(r'^play/(?P<game_token>[a-f0-9]+)/(?P<bot_token>[a-f0-9]+)/verif/?$', 'verif', name='verif'),
    url(r'^incidents/(?P<game_token>[a-z0-9]+)/?$', 'game_incidents', name='game_incidents'),
    url(r'^incidents/?$', 'incidents', name='incidents'),
    url(r'^games/?$', 'games', name='games'),
    url(r'^shortcuts/?$', 'shortcuts', name='shortcuts'),
    url(r'^visu/(?P<game_token>[a-f0-9]+)?$', 'visu', name='visu'),
)

