# -*- coding: utf8 -*-
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, JsonResponse

from arena.models import *
from arena.forms import *

import datetime as dt
import json

def register(request):
    """
    Registration form view

    Must display a form to register a new bot on a team or a new team
    """
    team_form = TeamForm()
    bot_form = BotForm()
    return render(request, 'register.html', {'team_form': team_form, 'bot_form': bot_form})

def register_bot(request):
    """
    Bot registration

    Must serve POST requests only, GET must be redirected to /api/register
    This view must process information from the registration form for a bot (including the verification of team's
    secret).

    The rendered output is HTML with bot token and bot secret
    """
    if not request.method == 'POST':
        return HttpResponseRedirect(reverse('register'))

    else:
        form = BotForm(request.POST)

        if form.is_valid():

            # retrieve team_object
            team = Team.objects.get(team_secret=form.cleaned_data['team_secret'])
            if not team:
                return render(request,
                        'register_fail.html',
                        {
                            'type': 'bot',
                            'form': form,
                            'message': 'unknown token'
                        })

            bot = Bot(
                    team=team,
                    name=form.cleaned_data['name']
                    )

            (token, secret) = bot.generate_credentials()

            bot.save()

            return render(request, 'register_success.html',
                    {
                        'type':'bot',
                        'bot_token': bot.token,
                        'bot_secret': bot.secret,
                    })

        else:
            return render(request,
                    'register_fail.html',
                    {
                        'type': 'bot',
                        'form': form,
                        'message': 'invalid_form'
                    })


def register_team(request):
    """
    Team registration

    Must serve POST requests only, GET must be redirected to /api/register
    This view must process information from the registration form for a new team

    The rendered output is HTML with team secret
    """
    if not request.method == 'POST':
        return HttpResponseRedirect(reverse('register'))

    else:
        form = TeamForm(request.POST)

        if form.is_valid():

            # retrieve team_object
            team = Team(
                    name=form.cleaned_data['name']
                    )

            secret = team.generate_credentials()

            team.save()

            return render(request, 'register_success.html',
                    {
                        'type':'team',
                        'team_secret': team.team_secret,
                    })

        else:
            return render(request,
                    'register_fail.html',
                    {
                        'type': 'team',
                        'form': form,
                        'message': 'invalid_form'
                    })


@csrf_exempt
def connect(request, bot_token):
    """
    Game connection view

    This view is the trickiest. Actually, we should implement some kind of keep-alive stuff to get the bot to wait for
    game start.

    This view also works as a dispatcher for bots into available games.
    """
    if not request.method == 'POST':
        return HttpResponseBadRequest(
            json.dumps({
                'success': False,
                'code': 400,
                'status': 'wrong_method',
                'error': 'Please use post requests. See documentation.'}),
            content_type="application/json"
        )

    else:
        # process data from response to json object
        try:
            json_data = json.loads(request.body)
        except:
            return HttpResponseBadRequest(
                json.dumps({
                    'success': False,
                    'code': 400,
                    'status': 'unreadable_object',
                    'error': 'Unparseable JSON data. Please recheck your json.'}),
                content_type="application/json"
            )

        # retrieve bot_object and check if its secret token is valid
        bot = Bot.objects.get(token=bot_token)

        # The bot does not exist
        if not bot:
            response = {
                    'success':False,
                    'status':'unknown_bot',
                    'message':'Sorry, but this bot is unknown! Did you register it?'
                    }
            return JsonResponse(response)

        # are all the required fields present ?
        if not json_data.get('secret') and not json_data.get('mode'):
            return HttpResponseBadRequest(
                json.dumps({
                    'success': False,
                    'code': 400,
                    'status': 'incomplete_object',
                    'error': 'All the required fields are not present'}),
                content_type="application/json"
            )

        # The secret token is not valid
        if json_data['secret'] != bot.secret:
            response = {
                    'success':False,
                    'status':'invalid_secret_token',
                    'message':'The secret token is not valid!'
                    }
            return JsonResponse(response)

        # Change the bot status according to the requested mode ("training" or "arena")
        mode = json_data['mode']
        if mode == "training":
            bot.state = Bot.OFFLINE
        elif mode == "arena":
            bot.state = Bot.READY
        elif mode == "eval":
            g = Game(
                    dtstart=dt.datetime(hour=10, minute=42, month=1, day=18, year=2015),
                    first_stop=Stop.objects.get(id=1091),
                    initial_arrival=Stop.objects.get(id=1027),
                    mode='training'
                    )
            g.save()
        else:
            response = {
                    'success':False,
                    'status':'invalid_mode',
                    'message':'The requested mode is unknown!'
                    }
            return JsonResponse(response)

        bot.save()

        registered = False
        if json_data['mode']=='arena':
            games_count = Game.objects.filter(num_bots__lt=3, mode='arena').count()
            if not games_count:
                g = Game.create_new(mode='arena')
                g.generate_incident(n=random.randint(8,15))

            else:
                g = Game.objects.filter(num_bots__lt=3, mode='arena')[random.randint(0,games_count-1)]
                while bot in g.bots.all():
                    g = Game.objects.filter(num_bots__lt=3, mode='arena')[random.randint(0,games_count-1)]

        else:
            if json_data.get('from_stop') and json_data.get('to_stop'):
                g = Game(
                        dtstart=dt.datetime.today(),
                        first_stop=Stop.objects.get(id=json_data.get('from_stop')),
                        initial_arrival=Stop.objects.get(id=json_data.get('to_stop')),
                        mode='training'
                        )
                g.save()
            else:
                g = Game.create_new(mode='training')

            if json_data.get('incidents')==False:
                pass
            elif json_data.get('mode')=="eval":
                dts = dt.datetime(2015, 01,01)
                dte = dt.datetime(2015,01,31)
                Incident(game=g, stop=Stop.objects.get(name="Prefecture"), type=Incident.MANIF, dtstart=dts, dtend=dte)
                Incident(game=g, stop=Stop.objects.get(name="Novaxis"), type=Incident.TRAVAUX, dtstart=dts, dtend=dte)
                Incident(game=g, stop=Stop.objects.get(name="Theodore monod"), type=Incident.MANIF, dtstart=dts, dtend=dte)
                Incident(game=g, stop=Stop.objects.get(name="Lefaucheux"), type=Incident.ACCIDENT, dtstart=dts, dtend=dte)
                Incident(game=g, stop=Stop.objects.get(name="Preau"), type=Incident.MALAISE, dtstart=dts, dtend=dte)
                Incident(game=g, stop=Stop.objects.get(name="Jaures-pavillon"), type=Incident.PANNE, dtstart=dts, dtend=dte)
                Incident(game=g, stop=Stop.objects.get(name="Gymnase"), type=Incident.MALAISE, dtstart=dts, dtend=dte)
                Incident(game=g, stop=Stop.objects.get(name="Kennedy"), type=Incident.PANNE, dtstart=dts, dtend=dte)
                Incident(game=g, stop=Stop.objects.get(name="Arromanches"), type=Incident.MALAISE, dtstart=dts, dtend=dte)
            else:
                g.generate_incident(n=random.randint(8,15))

            g.state = Game.STARTED

        g.num_bots += 1
        g.save()
        if g.num_bots==3: 
            g.state = Game.STARTED
            g.save()
        b2g = Bot2Game(game=g, bot=bot)
        b2g.position = g.first_stop
        b2g.time = g.dtstart
        b2g.target = g.initial_arrival
        b2g.save()

        response = {
                'success':True,
                'status':'registered',
                'message':'Your bot is now registered! Check the url to see when you will play.',
		'game_token': g.game_token,
                'url': reverse('verif', args=(g.game_token, bot.token))
                }
        return JsonResponse(response)


@csrf_exempt
def verif(request, game_token, bot_token):
    """
    Check view

    Here, we return the time remaining before the game starts.

    Response is JSON.
    """
    if not request.method == 'POST':
        return HttpResponseBadRequest(
            json.dumps({
                'success': False,
                'code': 400,
                'status': 'wrong_method',
                'error': 'Please use post requests. See documentation.'}),
            content_type="application/json"
        )

    # process data from response to json object
    try:
        json_data = json.loads(request.body)
    except:
        return HttpResponseBadRequest(
            json.dumps({
                'success': False,
                'code': 400,
                'status': 'unreadable_object',
                'error': 'Unparseable JSON data. Please recheck your json.'}),
            content_type="application/json"
        )

    # retrieve bot_object and check if its secret token is valid
    bot = Bot.objects.get(token=bot_token)

    # The bot does not exist
    if not bot:
        response = {
                'success':False,
                'status':'unknown_bot',
                'message':'Sorry, but this bot is unknown! Did you register it?'
                }
        return JsonResponse(response)

    # are all the required fields present ?
    if not json_data.get('secret_token'):
        return HttpResponseBadRequest(
            json.dumps({
                'success': False,
                'code': 400,
                'status': 'incomplete_object',
                'error': 'All the required fields are not present'}),
            content_type="application/json"
        )

    # The secret token is not valid
    if json_data['secret_token'] != bot.secret:
        response = {
                'success':False,
                'status':'invalid_secret_token',
                'message':'The secret token is not valid!'
                }
        return JsonResponse(response)

    # retrieve game_object
    game = Game.objects.get(game_token=game_token)

    if not game:
        response = {
                'success': False,
                'time':'-1',
                'status':'unknown_game',
                'message':'Sorry, but this game does not exist!'
                }
        return HttpResponseBadRequest(json.dumps(response), content_type="application/json")

    # check if the bot is known by the game
    engaged = Bot2Game.objects.filter(game=game, bot=bot).count()
    if not engaged:
        response = {
                'success': False,
                'time':'-1',
                'status':'bot_not_in_game',
                'message':'Sorry, but your bot is not engaged in this game!'
                }
        return HttpResponseBadRequest(json.dumps(response), content_type="application/json")

    if game.state==Game.PENDING:
        delta_t = datetime.datetime.now() - game.timestamp.replace(tzinfo=None)
        # remaining time = verification interval - Δ(now, last game recheck) - transfert time correction
        remaining = 30 - delta_t.total_seconds() - 1
        response = {
                'success': True,
                'time': remaining,
                'status':'game_pending',
                'message':'Game start is pending, please recheck soon.',
                'url': reverse('verif', args=(game.game_token, bot.token))
                }

    elif game.state==Game.STARTED:
        response = {
                'success': True,
                'time': 0,
                'status':'game_started',
                'message':'The game has started !',
                'first_stop': {'id': game.first_stop.id, 'name': game.first_stop.name},
                'target': {'id': game.initial_arrival.id, 'name': game.initial_arrival.name},
                'dtstart': game.dtstart.isoformat(),
                'url': reverse('move', args=(game.game_token, bot.token))
                }
    else:
        response = {
                'success': True,
                'time': -1,
                'status':'game_finished',
                'message':'The game is already finished...'
                }
    return JsonResponse(response)


@csrf_exempt
def move(request, game_token, bot_token):
    """
    Movement view

    Here, we just verify the game token and bot token against what we have in DB (we use the POST transmitted
    bot_secret to check the last one).

    If all the authentication stuff is OK, we proceed to move legality check against known passage times and incidents.

    All the "hard" work is done here.

    Response is JSON (we must acknowledge the move if it's correct or notify a penalty)
    """
    if not request.method == 'POST':
        return HttpResponseBadRequest(
            json.dumps({
                'success': False,
                'code': 400,
                'status': 'wrong_method',
                'error': 'Please use post requests. See documentation.'}),
            content_type="application/json"
        )

    # process data from response to json object
    try:
        json_data = json.loads(request.body)
    except:
        return HttpResponseBadRequest(
            json.dumps({
                'success': False,
                'code': 400,
                'status': 'unreadable_object',
                'error': 'Unparseable JSON data. Please recheck your json.'}),
            content_type="application/json"
        )

    # retrieve bot_object and check if its secret token is valid
    bot = Bot.objects.filter(token=bot_token)

    # The bot does not exist
    if bot.count() == 0:
        response = {
                'success':False,
                'status':'unknown_bot',
                'message':'Sorry, but this bot is unknown! Did you register it?'
                }
        return JsonResponse(response)

    bot = bot[0]

    # are all the required fields present ?
    if not\
       json_data['secret_token'] and\
       json_data['track'] and\
       json_data['connection'] and\
       json_data['to_stop'] and\
       json_data['type']:
        return HttpResponseBadRequest(
            json.dumps({
                'success': False,
                'code': 400,
                'status': 'incomplete_object',
                'error': 'All the required fields are not present.'}),
            content_type="application/json"
        )

    # The secret token is not valid
    if json_data['secret_token'] != bot.secret:
        response = {
                'success':False,
                'status':'invalid_secret_token',
                'message':'The secret token is not valid!'
                }
        return JsonResponse(response)

    # retrieve game_object
    game = Game.objects.get(game_token=game_token)

    if not game:
        response = {
                'success': False,
                'status':'unknown_game',
                'message':'Sorry, but this game does not exist!'
                }
        return HttpResponseBadRequest(json.dumps(response), content_type="application/json")

    # check if the bot is known by the game
    engaged = Bot2Game.objects.filter(game=game, bot=bot).count()
    if not engaged:
        response = {
                'success': False,
                'status':'bot_not_in_game',
                'message':'Sorry, but your bot is not engaged in this game!'
                }
        return HttpResponseBadRequest(json.dumps(response), content_type="application/json")

    # IS THE MOVE VALID? (i.e. the track exists AND the stop exists AND the connection exists)

    MOVE_PENALTIES = dict((
        ("BAD_TRACK",30),
        ("BAD_STOP",0),           #0min but the bot is moved to the end of the current track
        ("BAD_CONNECTION",20),
        ("PASTED_CONNECTION",5),
        ("BAD_SHORTCUT",40),
        ("DEFAULT", 30)
    ))

    b2g = Bot2Game.objects.get(game=game, bot=bot)

    mov = Move(game=game,bot=bot)
    mov.date_played = b2g.time
    mov.track = Track.objects.get(track_number='NULL')

    track_number = json_data.get('track')
    t = Track.objects.filter(track_number=track_number)

    # does the track exist?
    if t.count() == 0:

        # the bot does not move and loses 30min.
        mov.label = 'Penalty for bad track.'
        mov.from_stop = ChainedStop.objects.get(track__track_number="NULL", stop=b2g.position)
        mov.to_stop = ChainedStop.objects.get(track__track_number="NULL", stop=b2g.position)
        mov.timelength = MOVE_PENALTIES['BAD_TRACK']
        mov.save()

        b2g.time += dt.timedelta(seconds=mov.timelength*60)
        b2g.save()

        response = {
                'success':False,
                'status':'unknown_track',
                'penalty': MOVE_PENALTIES['BAD_TRACK'],
                'message':'Sorry, but this track does not exist or you are not allow to take it. Consequently, your bot has been penalized.',
                'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                'time': b2g.time.isoformat()
                }
        return JsonResponse(response)

    t = t[0]

    # the track exists, we save the information
    mov.track = t

    from_stop = ChainedStop.objects.filter(track=t, stop=b2g.position)
    if from_stop.count() == 0:

        # the requested stop is not valid, we move the bot to the end of the track
        from_stop = ChainedStop.objects.get(track__track_number="NULL", stop=b2g.position)

        mov.label = 'Penalty for bad track.'
        mov.from_stop = from_stop
        mov.to_stop = from_stop
        mov.timelength = MOVE_PENALTIES['BAD_TRACK']
        mov.save()

        b2g.time += dt.timedelta(seconds=mov.timelength*60)
        b2g.save()

        response = {
                'success':False,
                'status':'bad_track',
                'penalty': MOVE_PENALTIES['BAD_TRACK'],
                'message':"Sorry, but the requested track does not go through the stop you were at. You've been penalized",
                'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                'time': b2g.time.isoformat()
                }
        return JsonResponse(response)
    from_stop = from_stop[0]

    to_stop = ChainedStop.objects.filter(stop__id=json_data.get('to_stop'), track=t)      # arrival
    if to_stop.count() == 0:

        # the requested stop is not valid, we move the bot to the end of the track
        to_stop = ChainedStop.objects.get(track__track_number="NULL", stop=t.direction)

        mov.label = 'Penalty for bad stop.'
        mov.from_stop = from_stop
        mov.to_stop = to_stop
        mov.timelength = MOVE_PENALTIES['BAD_STOP']
        mov.save()

        b2g.position = to_stop.stop
        b2g.time += dt.timedelta(seconds=mov.timelength*60)

        b2g.save()

        response = {
                'success':False,
                'status':'bad_stop',
                'penalty': MOVE_PENALTIES['BAD_STOP'],
                'message':'Sorry, but this stop does not belong to this track, your move is not valid. Consequently, your bot has been penalized, your current position is now the last stop of your direction.',
                'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                'time': b2g.time.isoformat()
                }
        return JsonResponse(response)

    to_stop = to_stop[0]

    # does the bot position allow to take the track?
    if not b2g.position in t.stops.all():

        # the bot does not move and loses 30min.
        mov.label = 'Penalty for bad track.'
        mov.from_stop = from_stop
        mov.to_stop = from_stop
        mov.timelength = MOVE_PENALTIES['BAD_TRACK']
        mov.save()

        b2g.time += dt.timedelta(seconds=mov.timelength*60)
        b2g.save()

        response = {
                'success':False,
                'status':'bad_track',
                'penalty': MOVE_PENALTIES['BAD_TRACK'],
                'message':'Sorry, but your current position is not on the requested track. Consequently, your bot has been penalized.',
                'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                'time': b2g.time.isoformat()
                }
        return JsonResponse(response)

    # THE MOVE IS VALID

    move_type = json_data.get('type')

    if move_type=="move":

        b2g_time = b2g.time.time()
        b2g_date = b2g.time.date()

        passage_dict = dict(Passage.DAYS)
        current_day = passage_dict['di']

        # check is the day format is correct
        connection = json_data.get('connection')
        connection_datetime = dt.datetime.strptime(connection,"%d %b %X %Y") # e.g. date = 13 Sep 13:20:00 2015"

        if not connection_datetime:

            mov.label = 'Penalty for bad date format.'
            mov.from_stop = from_stop
            mov.to_stop = from_stop
            mov.timelength = MOVE_PENALTIES['DEFAULT']
            mov.save()

            b2g.time += dt.timedelta(seconds=mov.timelength*60)
            b2g.save()

            response = {
                    'success':False,
                    'status':'connection_date_not_valid',
                    'penalty': MOVE_PENALTIES['DEFAULT'],
                    'message':'The connection date format is not correct. Consequently, your bot has been penalized.',
                    'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                    'time': b2g.time.isoformat()
                    }
            return JsonResponse(response)

        passage_dict = dict(Passage.DAYS)
        current_day = passage_dict[passage_dict.keys()[b2g.time.weekday()]]
        connection_day = passage_dict[passage_dict.keys()[connection_datetime.weekday()]]
        # connection_day = passage_dict[] # should be 'Di'
        connection_time = connection_datetime.time()

        # we retrieve the requested connection
        p = Passage.objects.filter(day='di',time=connection_time,stop=from_stop) #TODO: check if using connection_time like that works

        if p.count() == 0:

            mov.label = 'Penalty for invalid connection.'
            mov.from_stop = from_stop
            mov.to_stop = from_stop
            mov.timelength = MOVE_PENALTIES['BAD_CONNECTION']
            mov.save()

            b2g.time += dt.timedelta(seconds=mov.timelength*60)
            b2g.save()

            response = {
                    'success':False,
                    'status':'unknown_connection',
                    'penalty': MOVE_PENALTIES['BAD_CONNECTION'],
                    'message':'The requested connection does not exist. Consequently, your bot has been penalized.',
                    'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                    'time': b2g.time.isoformat()
                    }
            return JsonResponse(response)

        current_timestamp = b2g.time
        # check if the requested connection is not pasted yet
        if connection_day == current_day and connection_time <= current_timestamp.time():

            offset_pen = (dt.datetime.combine(dt.date.today(),current_timestamp.time()) -\
                          dt.datetime.combine(dt.date.today(),connection_time)).total_seconds()

            mov.label = 'Penalty for bad connection.'
            mov.from_stop = from_stop
            mov.to_stop = from_stop
            mov.timelength = offset_pen / 60
            mov.save()

            b2g.time += dt.timedelta(seconds=offset_pen)
            b2g.save()

            response = {
                    'success':False,
                    'status':'pasted_connection',
                    'penalty': mov.timelength,
                    'message':'The requested connection is pasted. Consequently, your bot has been penalized.',
                    'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                    'time': b2g.time.isoformat()
                    }
            return JsonResponse(response)

        time2connection = (connection_datetime - current_timestamp.replace(tzinfo=None))
        current_timestamp += dt.timedelta(seconds=time2connection.total_seconds())

        # The move and connection are both valids. We applied the move.
        first_stop = from_stop
        last_stop = to_stop       # where it wants to go

        # list of stops from the departure till the requested arrival
        ordered_stops = ChainedStop.objects.filter(track=t,
                position__gt=first_stop.position,
                position__lte=last_stop.position).order_by('position')

        total_penalty = 0
        time_without_penalty = 0

        penalty_dict = dict(Incident.PENALTIES)

        for s in ordered_stops:

            incidents = Incident.objects.filter(game=game, stop=s.stop)
            for i in incidents:
                if current_timestamp > i.dtstart and current_timestamp < i.dtend:
                    total_penalty += penalty_dict[i.type]*60

            ## we retrieve the passage right after
            #next_p = Passage.objects.filter(
            #        stop=s,
            #        day=passage_dict['di'],
            #        time__gt=current_timestamp.time()
            #).count()

            #if next_p != 0:
            #    p = Passage.objects.filter(
            #        stop=s,
            #        day=passage_dict['di'],
            #        time__gt=current_timestamp.time()
            #    ).order_by('time')[0]

            #    time_without_penalty = (dt.datetime.combine(current_timestamp.date(), p.time) - current_timestamp).total_seconds()

            #else:
            #    # there is no more service for the current day, so we move to the first connection the day after
            #    p = Passage.objects.filter(
            #            stop=s,
            #            day=passage_dict['di'],
            #    ).order_by('time')[0]

            #    p_date = (current_timestamp + dt.timedelta(days=1)).date()
            #    p_datetime = dt.datetime.combine(p_date,p.time)
            #    time_without_penalty = (p_datetime - current_timestamp).total_seconds()
    	    next_p = Passage.objects.filter(
                stop=s,
                day=Passage.DIMANCHE,
                time__gt=current_timestamp.time()
            ).order_by('time')

            if next_p.count() == 0:

                p = Passage.objects.filter(
                    stop=s,
                    day=Passage.DIMANCHE
                ).order_by('time')[0]

                p_date = (current_timestamp + dt.timedelta(days=1)).date()
                p_datetime = dt.datetime.combine(p_date,p.time)
                time_without_penalty = (p_datetime - current_timestamp).total_seconds()

            else:

                p = next_p[0]
                time_without_penalty = (dt.datetime.combine(current_timestamp.date(), p.time) - current_timestamp.replace(tzinfo=None)).total_seconds()


            current_timestamp += dt.timedelta(seconds=(time_without_penalty + total_penalty))

        mov.label = 'Valid move.'
        mov.from_stop = from_stop
        mov.to_stop = to_stop
        mov.timelength = (current_timestamp - b2g.time).total_seconds() / 60
        mov.save()

    elif move_type == "shortcut":

        shortcut = Shortcut.objects.filter(from_stop__id=from_stop, to_stop__id=to_stop)

        if shortcut.count() == 0:

            mov.label = 'Penalty for bad shortcut.'
            mov.from_stop = from_stop
            mov.to_stop = from_stop
            mov.timelength = MOVE_PENALTIES['BAD_SHORTCUT']
            mov.save()

            b2g.time += dt.timedelta(seconds=mov.timelength*60)
            b2g.save()

            response = {
                'success':False,
                'status':'bad_shortcut',
                'penalty': MOVE_PENALTIES['BAD_SHORTCUT'],
                'message':'The move format is correct but the shortcut is false. Consequently, your bot has been penalized.'
                }
            return JsonRequest(response)

        mov.label = 'Shortcut.'
        mov.from_stop = from_stop
        mov.to_stop = to_stop
        mov.timelength = shortcut.timelength
        mov.save()

        current_timestamp += dt.timedelta(seconds=shortcut.timelength*60)

    else:
        mov.label = 'Penalty for bad move type.'
        mov.from_stop = from_stop
        mov.to_stop = from_stop
        mov.timelength = MOVE_PENALTIES['DEFAULT']
        mov.save()

        b2g.time += dt.timedelta(seconds=mov.timelength*60)
        b2g.save()

        response = {
                'success':False,
                'status':'bad_move_type',
                'penalty': MOVE_PENALTIES['DEFAULT'],
                'message':'The type of your move is false. Consequently, your bot has been penalized.',
                'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                'time': b2g.time.isoformat()
                }
        return JsonResponse(response)

    b2g.position = mov.to_stop.stop
    b2g.time = current_timestamp
    b2g.save()

    # now the move is applied, we check if the target has been already reached by a bot
    # if True, the target has changed and we have to inform the bot
    finished = Bot2Game.objects.filter(game=game, arrived=True).count()

    if finished:
        response = {
                'success':True,
                'status':'rerouted',
                'target':b2g.target,
                'message':'The request was correct, your bot has moved. However it has been rerouted, the target has changed!',
                'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
                'time': b2g.time.isoformat()
                }
        return JsonResponse(response)

    # nobody has finished yet, we check if the bot reached its target.
    if b2g.position == b2g.target:

        b2g.arrived = True
        b2g.save()

        if not Bot2Game.objects.filter(game=game, arrived=False).count():
            game.state = Game.FINISHED
        else:
            game.new_target()

        b2g_movs = Move.objects.filter(game=game, bot=bot).all()
        b_playtime = 0

        for m in b2g_movs:
            b_playtime += m.timelength

        # bot score = running time (in minute) + (# of request * 7) + (# of rerouting * 10)
        b2g.score = (b_playtime * 60) + (b2g_movs.count() * 7) + (b2g.rerouted * 10)
        b2g.save()

        response = {
            'success':True,
            'status':'arrived',
            'score':b2g.score,
            'message':'Your bot has reached the target, congrats!'
            }
        return JsonResponse(response)

    response = {
            'success':True,
            'status':'moved',
            'message':'The request was correct. Your bot has moved.',
            'stop': {'id': mov.to_stop.stop.id, 'name': mov.to_stop.stop.name},
            'time': b2g.time.isoformat()
            }
    return JsonResponse(response)


def game_incidents(request, game_token):
    """
    Incidents view for a game

    Here, we retrieve a (JSON) list of incidents related to a particular game (based on the GET transmitted game token)
    """

    g = Game.objects.filter(game_token=game_token)
    if g.count==0:
        response = {
                'success': False,
                'status':'unknown_game',
                'message':'Sorry, but this game does not exist!'
                }
        return HttpResponseBadRequest(json.dumps(response), content_type="application/json")

    g = g[0]

    incidents = Incident.objects.filter(game__game_token=game_token)
    if len(incidents)==0:
        response = {
                'success': True,
                'status':'incident_list',
                'message':'Sorry, but this game has no incident !'
                }
        return JsonResponse(response)

    penalty_dict = dict(Incident.PENALTIES)

    response = {
        'success': True,
        'status': 'incident_list',
        'game_token': game_token,
        'incidents': [
            {
                'type': i.type,
                'penalty': penalty_dict[i.type],
                'dtstart': i.dtstart.isoformat(),
                'dtend': i.dtend.isoformat(),
                'stop': {
                    'id': i.stop.id,
                    'name': i.stop.name
                }
            }
        for i in incidents]
    }
    return JsonResponse(response)


def incidents(request):
    """
    Incidents view

    It's exactly the same than the previous view (game_incidents()), but this time we do transmit all the incidents
    relative to all games (JSON format).
    """

    penalty_dict = dict(Incident.PENALTIES)

    games = Game.objects.filter(state=Game.FINISHED, mode=Game.ARENA)

    response = {
        'success': True,
        'status': 'incident_list',
        'games': [{
            'game_token': g.game_token,
            'incidents': [
                {
                    'type': i.type,
                    'penalty': penalty_dict[i.type],
                    'dtstart': i.dtstart.isoformat(),
                    'dtend': i.dtend.isoformat(),
                    'stop': {
                        'id': i.stop.id,
                        'name': i.stop.name
                    }
                }
            for i in incidents]
        } for g in games]
    }
    return JsonResponse(response)

def games(request):
    """
    Games list view

    What we wanna do here is to provide a full list of games (JSON format).
    This view will probably only be used for HMI
    """

    games = Game.objects.filter(mode=Game.ARENA).exclude(state=Game.PENDING)

    response = {
        'success': True,
        'status': 'game_list',
        'games': [{
            'game_token': g.game_token,
            'bots': [
                {
                    'team_name': b.team.team_name,
                    'name': b.name
                }
            for b in g.bots]
        } for g in games]
    }
    return JsonResponse(response)

def shortcuts(request):
    """
    Shortcuts list view
    """

    games = Shortcut.objects.all()

    response = {
        'success': True,
        'status': 'shortcuts_list',
        'shortcuts': [{
            'from_stop': {'name': s.from_stop.stop.name, 'id': s.from_stop.stop.id},
            'to_stop': {'name': s.to_stop.stop.name, 'id': s.to_stop.stop.id},
            'timelength': s.timelength
        } for s in shortcuts]
    }
    return JsonResponse(response)

def visu(request, game_token):
    """
    Visualization view

    We send the caller back a full JSON object descripting the progress of the game related to the transmitted game
    token
    """

    try:
        g = Game.objects.get(game_token=game_token)
    except DoesNotExist:
        response = {
                'success': False,
                'status':'unknown_game',
                'message':'Sorry, but this game does not exist!'
                }
        return HttpResponseBadRequest(json.dumps(response), content_type="application/json")

    incidents = Incident.objects.filter(game=g)

    penalty_dict = dict(Incident.PENALTIES)

    moves = {}
    for b in g.bots.all():
        b_moves = Move.objects.filter(game=g, bot=b).order_by('date_played')

        moves[b.id] = [
            {
                'track': m.track,
                'from_stop': {'name': m.from_stop.stop.name, 'id': m.from_stop.stop.id},
                'to_stop': {'name': m.to_stop.stop.name, 'id': m.to_stop.stop.id},
                'timelength': m.timelength
            } for m in b_moves]

    response = {
        'success': True,
        'status': 'visualisation',
        'game_token': game_token,
        'first_stop': g.first_stop.name,
        'bots': [
            {
                'team_name': b.team.name,
                'name': b.name,
                'id': b.id
            } for b in g.bots.all()],
        'incidents': [
            {
                'type': i.type,
                'penalty': penalty_dict[i.type],
                'dtstart': i.dtstart.isoformat(),
                'dtend': i.dtend.isoformat(),
                'stop': {
                    'id': i.stop.id,
                    'name': i.stop.name
                }
            } for i in incidents],
        'moves': moves
    }
    return JsonResponse(response)

