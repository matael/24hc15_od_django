# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('token', models.CharField(max_length=100)),
                ('secret', models.CharField(max_length=100)),
                ('state', models.CharField(default=b'offline', max_length=7, choices=[(b'offline', b'Offline'), (b'ready', b'Ready'), (b'ingame', b'In Game')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Bot2Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('score', models.IntegerField(default=0)),
                ('arrived', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(to='arena.Bot')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChainedStop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('position', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dtstart', models.DateTimeField()),
                ('mode', models.CharField(max_length=8, choices=[(b'training', b'training'), (b'arena', b'arena')])),
                ('game_token', models.CharField(unique=True, max_length=100)),
                ('bots', models.ManyToManyField(to='arena.Bot', through='arena.Bot2Game')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Incident',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=10, choices=[(b'panne', b'Panne'), (b'malaise', b"Malaise d'un voyageur"), (b'accident', b'Accident de la circulation'), (b'travaux', b'Travaux sur les voies'), (b'manif', b'Manifestation !'), (b'greve', b'Gr\xc3\xa8ve du transporteur')])),
                ('dtstart', models.DateTimeField()),
                ('dtend', models.DateTimeField()),
                ('game', models.ForeignKey(to='arena.Game')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Move',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timelength', models.IntegerField()),
                ('date_played', models.DateTimeField()),
                ('bot', models.ForeignKey(to='arena.Bot')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Passage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.TimeField()),
                ('day', models.CharField(max_length=9, choices=[(b'lu', b'Lundi'), (b'ma', b'Mardi'), (b'me', b'Mercredi'), (b'je', b'Jeudi'), (b've', b'Vendredi'), (b'sa', b'Samedi'), (b'di', b'Dimanche')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Shortcut',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timelength', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Stop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('team_secret', models.CharField(unique=True, max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Track',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('track_number', models.IntegerField()),
                ('service', models.CharField(default=b'SETRAM', max_length=30, blank=True)),
                ('direction', models.ForeignKey(related_name='direction_of', to='arena.Stop')),
                ('stops', models.ManyToManyField(to='arena.Stop', through='arena.ChainedStop')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='shortcut',
            name='from_stop',
            field=models.ForeignKey(related_name='shortcut_from', to='arena.Stop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='shortcut',
            name='to_stop',
            field=models.ForeignKey(related_name='shortcut_to', to='arena.Stop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='passage',
            name='stop',
            field=models.ForeignKey(to='arena.Stop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='move',
            name='from_stop',
            field=models.ForeignKey(related_name='move_from', to='arena.Stop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='move',
            name='game',
            field=models.ForeignKey(to='arena.Game'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='move',
            name='to_stop',
            field=models.ForeignKey(related_name='move_to', to='arena.Stop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='incident',
            name='stop',
            field=models.ForeignKey(to='arena.Stop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='first_stop',
            field=models.ForeignKey(to='arena.Stop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='chainedstop',
            name='stop',
            field=models.ForeignKey(to='arena.Stop'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='chainedstop',
            name='track',
            field=models.ForeignKey(to='arena.Track'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bot2game',
            name='game',
            field=models.ForeignKey(to='arena.Game'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bot',
            name='team',
            field=models.ForeignKey(to='arena.Team'),
            preserve_default=True,
        ),
    ]
