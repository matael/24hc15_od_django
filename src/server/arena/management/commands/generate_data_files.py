#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# generate_data_files.py
#
# Copyright © 2015 Mathieu Gaborit (matael) <mathieu@matael.org>
#
#
# Distributed under WTFPL terms
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
#

from __future__ import print_function

from django.core.management.base import BaseCommand
from arena.models import Stop, Track, ChainedStop, Passage

import os
import sys
import time
import string
import json

class Command(BaseCommand):
    help = "Generate date files for every track a a general data file"


    def handle(self, *a, **kw):

        before = time.time()
        errors = 0

        tracks = Track.objects.all()
        n = tracks.count()

        BASEDIR = '/home/matael/workspace/projects/24hc15_od_django/src/server/'


        for t in tracks:

            # try:
            n_stops = t.stops.count()
            stops = ChainedStop.objects.filter(track=t).order_by('position')

            # stops_list = [
            #         {
            #             'id': s.stop.id,
            #             'name': s.stop.name,
            #             'position': s.position
            #             } for s in stops ]
            #
            stops_list = []
            for s in stops:
                stops_list.append({
                        'id': s.stop.id,
                        'name': s.stop.name,
                        'position': s.position
                        })

            schedule = {}
            for s in stops:
                schedule[s.stop.id] = {}
                for d in dict(Passage.DAYS).keys():
                    schedule[s.id][d] = [_.time.isoformat() for _ in Passage.objects.filter(stop=s, day='di').order_by('time')]


            data_for_track = {
                    'track_id': t.id,
                    'track_number': t.track_number,
                    'n_stops': n_stops,
                    'track_dest': {'id': t.direction.id, 'name': t.direction.name},
                    'stops': stops_list,
                    'schedule': schedule
            }

            with open(BASEDIR+t.track_number+'.json', 'w') as f:
                f.write(json.dumps(data_for_track))
            # except:
            #
            #     self.stdout.write('Erreur sur la ligne : {}'.format(t.track_number))
            #     errors += 1

        elapsed = time.time()-before

        self.stdout.write("{}/{} data files created without errors in {}s.".format(n-errors,n, elapsed))

