#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# load_stops.py
#
# Copyright © 2014 Mathieu Gaborit (matael) <mathieu@matael.org>
#
#
# Distributed under WTFPL terms
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
#

from django.core.management.base import BaseCommand
from arena.management.commands._utils import generate_stop_dict
from arena.models import Stop

import time

class Command(BaseCommand):
    help = "Load Stop objects into DB from the SETRAM dump"


    def handle(self, *a, **kw):

        before = time.time()
        i = 0
        keys = generate_stop_dict().keys()
        for k in keys:

            try:
                s = Stop(name=k).save()
                i += 1
            except:
                self.stdout.write("Error : {}".format(k))

        elapsed = time.time()-before

        self.stdout.write("Loaded {0}/{1} stops in {2}s".format(i,len(keys),elapsed))


