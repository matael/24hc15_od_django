# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('arena', '0002_game_num_bots'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='last_move',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 11, 22, 36, 36, 327803), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bot2game',
            name='target',
            field=models.ForeignKey(default=0, to='arena.Stop'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='game',
            name='initial_arrival',
            field=models.ForeignKey(related_name='initial_arrival', default=0, to='arena.Stop'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='game',
            name='state',
            field=models.CharField(default=b'pending', max_length=8, choices=[(b'pending', b'pending'), (b'started', b'started'), (b'finished', b'finished')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 11, 22, 37, 7, 399578), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='game',
            name='first_stop',
            field=models.ForeignKey(related_name='first_stop', to='arena.Stop'),
            preserve_default=True,
        ),
    ]
