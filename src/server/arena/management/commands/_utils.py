#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# _utils.py
#
# Copyright © 2014 Mathieu Gaborit (matael) <mathieu@matael.org>
#
#
# Distributed under WTFPL terms
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
#


import psycopg2 as psql

DB_HOST = "matael.org"
DB_NAME = "setram"
DB_USER = "test_24hc15"
DB_PASS = "24hc15_testrole"


def get_connection():
    return psql.connect(
            host=DB_HOST,
            user=DB_USER,
            password=DB_PASS,
            database=DB_NAME)


def generate_stop_dict():
        query = "SELECT ref,nom FROM arret"

        conn = get_connection()
        cur = conn.cursor()
        cur.execute(query)

        result_dict = {}

        for r in cur.fetchall():
            if result_dict.get(r[1]):
                result_dict[r[1]].append(r[0])
            else:
                result_dict[r[1]] = [r[0]]

        cur.close()
        conn.close()

        return result_dict
