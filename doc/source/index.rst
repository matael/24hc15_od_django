.. Urban Flow documentation master file, created by
   sphinx-quickstart on Wed Dec 31 16:44:33 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Urban Flow : "opération de secours"
===================================

    Pour toute info, demandez à un des gars du HAUM (tee-shirt noir avec HAUM imprimé).

Ton objectif si tu acceptes, est de participer en tant qu’agent de la
SETRAM à une opération de sauvetage après la perdition de trois des
membres du collectif “Urban Flow” qui se sont égarés dans le réseau de
transport en commun du Mans.

Ce collectif milite pour l’ouverture des données de transports et ces derniers se sont lancé le défi insensé de cartographier le réseau de transports en commun du Mans (i.e. les lignes, les correspondances, et les horaires) avec l’objectif d’en faire un jeu de donnée open-data.

Bien décidés à aller au bout mais sous équipés, ce sont leurs camarades d’Urban Flow qui ont donné l’alerte, ne les revoyant pas revenir.

Alors que deux membres sont toujours en cours de localisation, la troisième personne a elle été localisée.  Ta mission est donc d’aller à sa rencontre pour la ramener.

Comment jouer ?
===============

1. S'enregistrer

Rendez-vous sur la page register_ pour obtenir les identifiants qui seront nécessaires pour jouer.

2. Récupérer le jeu de données

Le jeu de données est disponible ici_ et sa description est là_.

3. Demander un jeu pour votre bot et attendre que la partie démarre

4. Une fois la partie démarrée, proposer un ensemble de mouvement nécessaire pour atteindre la cible!

Pour savoir comment réaliser ces actions, consultez l'API documentée :

.. toctree::
    :maxdepth: 2

    api

.. _register: http://24hc15.haum.org/api/register
.. _ici: _static/data_files.tgz
.. _là: http://haum.org/24hc15/data.html

