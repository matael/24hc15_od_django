Interface REST
==============

L'API utilise des requêtes GET et POST.

Les points d'entrée sont décrits juste après.

    Pensez à toujours tester le paramètre ``success`` de la réponse du serveur

Enregistrement
--------------

L'enregistrement se fait en allant sur l'API : http://24hc15.haum.org/api/register

Là, vous pouvez choisir d'enregistrer un bot ou une équipe. Dans l'ordre :

- enregistrez une équipe, gardez (précieusement) le token renvoyé
- enregistrez un (ou plusieurs) bot en enregistrant, là aussi les token et secret renvoyés.


Demande d'un jeu (POST)
-----------------------

Pour demander un jeu, utilisez l'URL : ``http://24hc15.haum.org/api/connect/<bot_token>``

Il faut envoyer un JSON en POST sous la forme :

.. code-block:: javascript

    {
        "secret": "<bot_secret>",
        "mode": "<mode>"
    }

Les ``bot_token`` et ``bot_secret`` sont bien évidement ceux distribués à l'enregistrement.
Le paramètre ``mode`` peut prendre 2 valeurs :

- ``arena`` pour un jeu en compétition (où vous jouez à 3)
- ``training`` pour un jeu d'entrainement où vous jouez seul

Pour un jeu d'entrainement, vous avez la possibilté de choisir vos points de départ et d'arrivée en ajoutant les champs suivants à votre requête :

.. code-block:: javascript

    {
        "from_stop": "<stop_id>",
        "to_stop": "<stop_id>"
    }

Vous pouvez également demander à ce qu'aucun incident ne soit généré dans votre jeu de test :

.. code-block:: javascript

    {
        "incidents": false, // False into JSON format
    }

La réponse du serveur sera de la forme :

.. code-block:: javascript

        {
            "status": "registered",
            "url": "/api/play/<game_token>/<bot_token>/verif",
            "message": "Your bot is now registered! Check the url to see when you will play.",
            "success": true,
            "game_token": <game_token>
        }

Le ``game_token`` est un identifiant unique **lié au jeu en cours**.

Vérification du début de jeu (POST)
-----------------------------------

Périodiquement vous devez aller sur l'URL renvoyée par ``connect`` pour vérifier si le jeu a commencé.

Il faut donc appeler : ``http://24hc15.haum.org/api/play/<game_token>/<bot_token>/verif``

.. code-block:: javascript

    {
        "secret_token": "<bot_secret>"
    }

Le ``bot_secret`` est celui qui vous est renvoyé à l'enregistrement.

Il y a deux réponses possibles :

Le jeu n'a pas encore démarré
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: javascript

    {
        'success': True,
        'time': remaining,
        'status':'game_pending',
        'message':'Game start is pending, please recheck soon.',
        'url': '/api/play/<game_token>/<bot_token>/verif'
    }

Le jeu a démarré
~~~~~~~~~~~~~~~~

.. code-block:: javascript

    {
        'success': True,
        'time': 0,
        'status':'game_started',
        'message':'The game has started !',
        'first_stop': {'id': id_du_depart, 'name': nom_du_depart},
        'target': {'id': id_de_l_arrivee, 'name': nom_de_l_arrivee},
        'dtstart': "YYYY-MM-DD "
        'url':  '/api/play/<game_token>/<bot_token>/move'
    }

Appliquer un mouvement (POST)
-----------------------------

Pour bouger le bot dans le jeu, on utilisera l'url de mouvement (elle est renvoyée par vérif lorsque le jeu a démarré) :
``http://24hc15.haum.org/api/play/<game_token>/<bot_token>/move``.

Le JSON à envoyer est de la forme suivante :

.. code-block:: javascript

    {
        "secret_token": <bot_secret>,
        "track": <track_number>, // ex : 17_A
        "connection": datetime_str, // forme : 13 Sep 13:20:00 2015
        "to_stop": stop_id,
        "type": "move"
    }

Le mouvement est valide
~~~~~~~~~~~~~~~~~~~~~~~

Plusieurs réponses sont possibles dans le cas d'un mouvement valide :

Le bot s'est déplacé :

.. code-block:: javascript

    {
        "success": True,
        "status": "moved",
        "message": "The request was correct. Your bot has moved.",
        "stop": {"id": <stop_id>, "name": <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

Le bot s'est déplacé mais sa cible a changé (i.e. un autre bot l'a atteint avant vous) :

.. code-block:: javascript

    {
        "success": True,
        "status": "rerouted",
        "target': <new_target>,
        "message": "The request was correct, your bot has moved. However it has been rerouted, the target has changed!",
        "stop": {"id": <stop_id>, "name": <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

Le bot s'est déplacé et a atteint la cible avant les autres :

.. code-block:: javascript

    {
        "success": True,
        "status": "arrived",
        "score": <bot_score>,
        "message": "Your bot has reached the target, congrats!"
    }

Le mouvement n'est pas valide
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorsqu'un mouvement n'est pas valide, le bot n'est pas autorisé à se déplacer et une pénalité est appliquée. La sévérité de celle-ci (exprimée en minute(s)) varie selon la raison pour laquelle le mouvement a été rejeté.
Plusieurs réponses sont ainsi possibles :

1. La ligne (track) demandée n'existe pas :

Le bot est pénalisé de **30 minutes**.

.. code-block:: javascript

    {
        'success': False,
        'status': 'unknown_track',
        'penalty': 30,
        'message':'Sorry, but this track does not exist or you are not allow to take it. Consequently, your bot has been penalized.',
        'stop': {'id': <stop_id>, 'name': <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

2. La ligne (track) demandée ne passe pas par l'arrêt où se trouve le bot.

Le bot est pénalisé de **30 minutes**.

.. code-block:: javascript

    {
        'success': False,
        'status': 'bad_track',
        'penalty': 30,
        'message':"Sorry, but the requested track does not go through the stop you were at. You've been penalized",
        'stop': {'id': <stop_id>, 'name': <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

3. L'arrêt demandé (stop) n'existe pas sur la ligne sur laquelle se trouve le bot.

Le bot est **envoyé en fin de ligne**. Ce cas est le seul pour lequel le bot se déplace bien que le mouvement soit invalide.

.. code-block:: javascript

    {
        'success': False,
        'status': 'bad_stop',
        'penalty': 0,
        'message': 'Sorry, but this stop does not belong to this track, your move is not valid. Consequently, your bot has been penalized, your current position is now the last stop of your direction.',
        'stop': {'id': <stop_id>, 'name': <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

4. La ligne (track) demandée ne peut être prise là où se trouve le bot :

Le bot est pénalisé de **30 minutes**.

.. code-block:: javascript

    {
        'success': False,
        'status': 'bad_track',
        'penalty': 30,
        'message': 'Sorry, but your current position is not on the requested track. Consequently, your bot has been penalized.',
        'stop': {'id': <stop_id>, 'name': <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

5. L'horaire de passage (connection) demandé n'est pas dans le bon format :

Le bot est pénalisé de **30 minutes**.

.. code-block:: javascript

    {
        'success': False,
        'status': 'connection_date_not_valid',
        'penalty': 30,
        'message': 'The connection date format is not correct. Consequently, your bot has been penalized.',
        'stop': {'id': <stop_id>, 'name': <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

6. L'horaire de passage (connection) demandé n'est pas valide :

Le bot est pénalisé de **20 minutes**.

.. code-block:: javascript

    {
        'success': False,
        'status': 'unknown_connection',
        'penalty': 20,
        'message': 'The requested connection is not valid. Consequently, your bot has been penalized.',
        'stop': {'id': <stop_id>, 'name': <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

7. L'horaire de passage (connection) est déjà passé ! :

Le bot est pénalisé du **nombre de minutes depuis que l'horaire est dépassé**.

.. code-block:: javascript

    {
        'success': False,
        'status':'pasted_connection',
        'penalty': <offset_time>,
        'message':'The requested connection is pasted. Consequently, your bot has been penalized.',
        'stop': {'id': <stop_id>, 'name': <stop_name>},
        "time": <datetime_str> //format: ISO 8601
    }

Obtenir la liste des incidents (GET)
------------------------------------

À la création d'un jeu, une liste d'incidents est aléatoirement générée.
Pour consulter cette liste, utilisez l'url : ``http://24hc15.haum.org/api/incidents/<game_token>``.

Exemple d'une liste d'incidents :

.. code-block:: javascript

    {

        "status": "incident_list",
        "incidents":
        [{
        "penalty": 60,
        "stop":
        {
            "id": <stop_id>,
            "name": <stop_name>
        },
        "dtend": datetime_str,
        "dtstart": datetime_str,
        "type": "accident"
        },
        {
        "penalty": 120,
        "stop":
        {
            "id": <stop_id>,
            "name": <stop_name>
        },
        "dtend": datetime_str,
        "dtstart": datetime_str,
        "type": "travaux"
        },
        {
        "penalty": 10,
        "stop":
        {
            "id": <stop_id>,
            "name": <stop_name>
        },
        "dtend": datetime_str,
        "dtstart": datetime_str,
        "type": "malaise"
        },
        {
        "penalty": <stop_id>,
        "stop":
        {
            "id": <stop_id>,
            "name": <stop_name>
        },
        "dtend": datetime_str,
        "dtstart": datetime_str,
        "type": "manif"
        },
        {
        "penalty": 480,
        "stop":
        {
            "id": <stop_id>,
            "name": <stop_name>
        },
        "dtend": ,
        "dtstart": datetime_str,
        "type": "greve"
        }],
        "game_token": <game_token>,
        "success": true
        }

    }

