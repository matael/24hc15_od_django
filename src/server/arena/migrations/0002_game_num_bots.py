# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('arena', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='num_bots',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
