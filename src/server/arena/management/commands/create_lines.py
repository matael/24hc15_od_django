#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# create_lines.py
#
# Copyright © 2015 Mathieu Gaborit (matael) <mathieu@matael.org>
#
#
# Distributed under WTFPL terms
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
#

from __future__ import print_function

from django.core.management.base import BaseCommand
from arena.models import Stop, Track, ChainedStop, Passage

import os
import sys
import time
import string

class Command(BaseCommand):
    help = "Recreate lines from re-ordered timeo scrapping"


    def handle(self, *a, **kw):

        before = time.time()
        errors = 0
        n = 0

        BASEDIR = '../../scripts/load_stops_data/'
        files = [_ for _ in os.listdir(BASEDIR) if _[0]!='_']

        self.stdout.write('=> Loading tracks from {}*'.format(BASEDIR))
        for f in files:

            with open(BASEDIR+f) as fh:

                lines = fh.readlines()

            plines = [filter(lambda x: x in string.printable, _) for _ in lines]
            track_num = lines[0].split(':')[1].rstrip()
            sens = lines[1].split(':')[1].rstrip()
            dest = lines[2].split(':')[1].rstrip()

            ptrack_num = plines[0].split(':')[1].rstrip()
            psens = plines[1].split(':')[1].rstrip()
            pdest = plines[2].split(':')[1].rstrip()

            try:
                stops = [_.split(':')[1].rstrip() for _ in lines[3:]]
            except IndexError:
                print('[ERROR] IndexError on track {}_{}'.format(ptrack_num, psens))

            self.stdout.write('\tLoading track : {0}_{1} -> {2}'.format(ptrack_num, psens, pdest))

            dest_stop = Stop.objects.get(name=stops[-1].capitalize())
            t = Track(
                track_number=track_num+'_'+sens,
                direction=dest_stop,
                service='SETRAM'
            )
            t.save()

            i = 1
            for s in stops:
                assoc_stop = Stop.objects.get(name=s.capitalize())
                c = ChainedStop(
                    stop=assoc_stop,
                    track=t,
                    position=i
                )
                c.save()
                print('.',end='')
                i += 1

            print(' ')
            n+=1

        elapsed = time.time()-before

        self.stdout.write("{}/{} track loaded without errors in {}s.".format(n-errors,n, elapsed))






