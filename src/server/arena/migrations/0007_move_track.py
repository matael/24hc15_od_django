# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('arena', '0006_auto_20150115_1054'),
    ]

    operations = [
        migrations.AddField(
            model_name='move',
            name='track',
            field=models.ForeignKey(default=0, to='arena.Track'),
            preserve_default=False,
        ),
    ]
